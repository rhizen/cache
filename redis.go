package cache

import (
	"sync"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
)

type RedisCache struct {
	pool              *redis.Pool
	timeout           time.Duration
	defaultExpiration time.Duration
}
type Doer func() (string, []interface{})

func NewRedisCache(addr, password string) (*RedisCache, error) {
	rc := &RedisCache{
		timeout:           5 * time.Second,
		defaultExpiration: 1 * time.Hour,
	}
	var ping Doer = func() (string, []interface{}) {
		return "PING", nil
	}
	var pool = &redis.Pool{
		MaxIdle:     5,
		IdleTimeout: 2 * time.Minute,
		Dial: func() (redis.Conn, error) {
			optionDb := redis.DialDatabase(1)
			optionPwd := redis.DialPassword(password)
			c, err := redis.Dial("tcp", addr, optionDb, optionPwd)
			if err != nil {
				return nil, err
			}

			// check with PING
			if _, err := c.Do(ping()); err != nil {
				c.Close()
				return nil, err
			}

			return c, err
		},
		// custom connection test method
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}

			_, err := c.Do(ping())
			return err
		},
	}
	rc.pool = pool

	return rc, nil
}

type item struct {
	mu      sync.Mutex
	keys    []string
	data    map[string]interface{}
	expires time.Duration
}

func (i item) Get() ([]string, map[string]interface{}) {
	return i.keys, i.data
}

func (i item) Set(key string, val interface{}) {
	i.mu.Lock()
	defer i.mu.Unlock()
	if _, ok := i.data[key]; !ok {
		i.keys = append(i.keys, key)
	}
	i.data[key] = val
}

func (i item) IsMiss() bool {
	if i.data == nil {
		return true
	}

	return false
}

func (i item) Expires() time.Duration {
	return i.expires
}

func (i item) ExpiresAfter(d time.Duration) {
	i.expires = d
}

func (r *RedisCache) Save(item CacheItem) error {
	keys, data := item.Get()
	if len(data) == 0 || len(keys) == 0 || len(data) != len(keys) {
		return errors.New("saving data failed, no data exists")
	}
	conn := r.pool.Get()
	defer conn.Close()

	set := func(k string, v interface{}) Doer {
		var command = "SET"
		var args = []interface{}{k, v}
		if item.Expires() > 0 {
			command = "SETEX"
			args = append(args, item.Expires())
		}
		return func() (string, []interface{}) {
			return command, args
		}
	}

	if len(data) == 1 && len(keys) == 1 {
		key := keys[0]
		cmd, args := set(key, data[key])()
		_, err := redis.DoWithTimeout(conn, r.timeout, cmd, args...)
		return err
	}

	conn.Send("MULTI")
	for _, key := range keys {
		conn.Send(set(key, data[key])())
	}
	_, err := redis.DoWithTimeout(conn, r.timeout, "EXEC")
	return err
}

func (r *RedisCache) GetItem(key string) (CacheItem, error) {
	conn := r.pool.Get()
	defer conn.Close()
	i := item{
		keys: []string{key},
		data: map[string]interface{}{
			key: nil,
		},
	}
	raw, err := redis.DoWithTimeout(conn, r.timeout, "GET", key)
	if err != nil {
		return i, err
	}
	i.data[key] = raw

	return i, nil
}
