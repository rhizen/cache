package cache

import (
	"time"
)

type CacheItem interface {
	Set(key string, val interface{})
	Get() ([]string, map[string]interface{})
	IsMiss() bool
	ExpiresAfter(d time.Duration)
	Expires() time.Duration
}

type Cacher interface {
	Save(item CacheItem) error
	GetItem(key string) (CacheItem, error)
}
